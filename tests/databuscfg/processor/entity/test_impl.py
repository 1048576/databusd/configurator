from unittest import skip

from databuscfg.processor.entity.impl import EntityProcessorImpl
from databuscfg.validator.collection.impl import CollectionValidatorImpl
from databuscfg.validator.route.impl import RouteValidatorImpl
from pygentestcase.impl import TestCaseImpl
from pygentree.impl import TreeImpl
from pygentype.exception import ExpectedException


@skip("")
class EntityProcessorTest(TestCaseImpl):
    def test_process_undefined_route(self) -> None:
        route_id = self.randomText()
        entity_id = self.randomText()

        entity_tree = TreeImpl.create({
            "title": "",
            "wiki": "",
            "refs": [],
            "routes": [
                {
                    "route": route_id
                }
            ]
        })

        route_validator = RouteValidatorImpl(set())
        collection_validator = CollectionValidatorImpl(set())
        processor = EntityProcessorImpl(route_validator, collection_validator)

        err_ctx = self.assertRaises(ExpectedException)
        with (err_ctx):
            processor.process(entity_id, entity_tree)

        err_msg_template = (
            "Entity [{}]:\n"
            "  Route [{}]:\n"
            "    Route profile not found"
        )

        self.assertEqual(
            first=err_msg_template.format(
                entity_id,
                route_id
            ),
            second=str(err_ctx.exception)
        )

    def test_process_undefined_collection(self) -> None:
        collection_id = self.randomText()
        entity_id = self.randomText()

        entity_tree = TreeImpl.create({
            "title": "",
            "wiki": "",
            "refs": [],
            "routes": [
                {
                    "collection": collection_id
                }
            ]
        })

        route_validator = RouteValidatorImpl(set())
        collection_validator = CollectionValidatorImpl(set())
        processor = EntityProcessorImpl(route_validator, collection_validator)

        err_ctx = self.assertRaises(ExpectedException)
        with (err_ctx):
            processor.process(entity_id, entity_tree)

        err_msg_template = (
            "Entity [{}]:\n"
            "  Collection [{}]:\n"
            "    Collection profile not found"
        )
        self.assertEqual(
            first=err_msg_template.format(
                entity_id,
                collection_id
            ),
            second=str(err_ctx.exception)
        )
