from unittest import skip

from databuscfg.processor.collection.impl import CollectionProcessorImpl
from databuscfg.validator.route.impl import RouteValidatorImpl
from pygentestcase.impl import TestCaseImpl
from pygentree.impl import TreeImpl
from pygentype.exception import ExpectedException


@skip("")
class CollectionProcessorTest(TestCaseImpl):
    def test_process_undefined_route(self) -> None:
        route_id = self.randomText()
        entity_id = self.randomText()

        collection_tree = TreeImpl.create({
            "routes": [
                {
                    "route": route_id
                }
            ]
        })

        route_validator = RouteValidatorImpl(set())
        processor = CollectionProcessorImpl(route_validator)

        err_ctx = self.assertRaises(ExpectedException)
        with (err_ctx):
            processor.process(entity_id, collection_tree)

        err_msg_template = (
            "Collection [{}]:\n"
            "  Route [{}]:\n"
            "    Route profile not found"
        )

        self.assertEqual(
            first=err_msg_template.format(
                entity_id,
                route_id
            ),
            second=str(err_ctx.exception)
        )
