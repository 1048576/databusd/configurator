#!/bin/bash

set -o errexit
set -o nounset
set -o pipefail

function _sh {
    local command=$1
    echo -e "\e[1;34m$(pwd)\e[32m\$ ${command}\e[39m"
    sh -c "${command}"
}

_sh "mvn dependency:go-offline --batch-mode -Djava.net.useSystemProxies=true -Pdependencies"

# Clean
_sh "rm $0"
