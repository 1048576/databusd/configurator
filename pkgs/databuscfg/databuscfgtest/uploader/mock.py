from __future__ import annotations

from typing import Dict

from databuscfg.uploader.abstract import Uploader


class UploaderMock(Uploader):
    @staticmethod
    def create() -> UploaderMock:
        return UploaderMock("/", {})

    _bucket: str
    _items: Dict[str, str]

    def __init__(self, bucket: str, items: Dict[str, str]) -> None:
        self._bucket = bucket
        self._items = items

    def upload(self, item_id: str, item: str) -> None:
        self._items[self._bucket + item_id] = item

    def bucket(self, bucket_id: str) -> Uploader:
        return UploaderMock(
            bucket="{}{}/".format(self._bucket, bucket_id),
            items=self._items
        )

    def items(self) -> Dict[str, str]:
        return self._items
