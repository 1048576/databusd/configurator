from typing import Dict
from typing import Iterable
from typing import Tuple

from databuscfg.processor.abstract import ItemProcessor
from databuscfg.processor.abstract import ItemsProcessor
from databuscfg.processor.abstract import ItemType
from databuscfg.processor.exception import ItemAlreadyDefinedException
from pygentree.abstract import Tree
from pygentype.exception import ExpectedException


class ItemsProcessorImpl(ItemsProcessor[ItemType]):
    _collection_name: str
    _item_name: str
    _item_processor: ItemProcessor[ItemType]

    def __init__(
        self,
        collection_name: str,
        item_name: str,
        item_processor: ItemProcessor[ItemType]
    ) -> None:
        self._collection_name = collection_name
        self._item_name = item_name
        self._item_processor = item_processor

    def process(
        self,
        items: Iterable[Tuple[str, Tree]]
    ) -> Dict[str, ItemType]:
        collection: Dict[str, ItemType] = {}
        try:
            for item_id, tree in items:
                item = self._item_processor.process(item_id, tree)
                if (item_id in collection):
                    raise ItemAlreadyDefinedException.create(
                        item_name=self._item_name,
                        item_id=item_id
                    )

                collection[item_id] = item
        except ExpectedException as e:
            raise e.wrap(self._collection_name)

        return collection
