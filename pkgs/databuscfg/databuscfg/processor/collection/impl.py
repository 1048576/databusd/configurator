from __future__ import annotations

from databuscfg.collection.abstract import ItemProcessor
from databuscfg.profile.collection.abstract import CollectionProfile
from databuscfg.profile.collection.impl import CollectionProfileImpl
from databuscfg.validator.route.impl import RouteValidatorImpl
from pygentree.abstract import Tree
from pygentype.exception import ExpectedException


class CollectionProcessorImpl(ItemProcessor[CollectionProfile]):
    _route_validator: RouteValidatorImpl

    def __init__(
        self,
        route_validator: RouteValidatorImpl
    ) -> None:
        self._route_validator = route_validator

    def process(
        self,
        unique_id: str,
        tree: Tree
    ) -> CollectionProfile:
        try:
            collection = CollectionProfileImpl.create(
                unique_id=unique_id,
                tree=tree
            )

            for collection_route in collection.routes():
                try:
                    self._route_validator.validate(
                        instance=collection_route.unique_id()
                    )
                except ExpectedException as e:
                    raise e.wrap(
                        title="Route [{}]".format(
                            collection_route.unique_id()
                        )
                    )

            return collection
        except ExpectedException as e:
            raise e.wrap(
                title="Collection [{}]".format(
                    unique_id
                )
            )
