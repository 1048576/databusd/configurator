from pygentree.abstract import Tree
from pygentree.impl import TreeImpl
from pygentype.exception import ExpectedException
from pygenyaml.impl import YamlLoaderImpl


class FileLoaderImpl:
    _path: str
    _err_msg_template: str

    def __init__(self, path: str, err_msg_template: str) -> None:
        self._path = path
        self._err_msg_template = err_msg_template

    def load(self) -> Tree:
        try:
            nodes = YamlLoaderImpl.create().load_from_file(self._path)
            try:
                return TreeImpl.create(nodes)
            except ExpectedException as e:
                raise e.wrap("Invalid file format")
        except ExpectedException as e:
            raise e.wrap(
                title=self._err_msg_template.format(
                    path=self._path
                )
            )
