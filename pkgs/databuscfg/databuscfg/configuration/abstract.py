
from typing import Dict
from typing import List

from databuscfg.profile.collection.abstract import CollectionProfile
from databuscfg.profile.entity.abstract import EntityProfile
from databuscfg.profile.route.abstract import RouteProfile
from databuscfg.profile.system.abstract import SystemProfile


class Configuration:
    def systems(self) -> Dict[str, SystemProfile]:
        ...

    def routes(self) -> Dict[str, RouteProfile]:
        ...

    def collections(self) -> Dict[str, CollectionProfile]:
        ...

    def entities(self) -> Dict[str, EntityProfile]:
        ...

    def entity_routes(self, entity: EntityProfile) -> List[RouteProfile]:
        ...
