from typing import Iterable
from typing import List

from databuscfg.profile.entity.abstract import EntityProfile
from databuscfg.profile.route.abstract import RouteProfile


class WikiConfiguration:
    def routes(self) -> Iterable[RouteProfile]:
        ...

    def entities(self) -> Iterable[EntityProfile]:
        ...

    def route_entities(
        self,
        route: RouteProfile
    ) -> List[EntityProfile]:
        ...

    def entity_routes(
        self,
        entity: EntityProfile
    ) -> List[RouteProfile]:
        ...
