package com.gitlab._1048576.libd.jvgen.kerberos;

public interface JvgenKerberosKeytabConfiguration {
    public String principal();

    public String filepath();
}
