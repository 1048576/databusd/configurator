package com.gitlab._1048576.libd.jvgen.httpserver.security.authenticator;

import com.gitlab._1048576.libd.jvgen.httpserver.security.JvgenHttpServerSecurityAuthenticator;
import com.gitlab._1048576.libd.jvgen.kerberos.JvgenKerberosKeytabConfiguration;
import jakarta.servlet.Filter;
import java.util.Collections;
import java.util.Map;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.kerberos.authentication.KerberosServiceAuthenticationProvider;
import org.springframework.security.kerberos.authentication.KerberosTicketValidator;
import org.springframework.security.kerberos.authentication.sun.SunJaasKerberosTicketValidator;
import org.springframework.security.kerberos.web.authentication.SpnegoAuthenticationProcessingFilter;

public final class JvgenHttpServerSecurityKerberosAuthenticatorImpl implements JvgenHttpServerSecurityAuthenticator {
    private final String principal;
    private final Resource keytab;
    private final UserDetailsService userDetailsService;

    public JvgenHttpServerSecurityKerberosAuthenticatorImpl(
        final JvgenKerberosKeytabConfiguration keytab,
        final UserDetailsService userDetailsService
    ) {
        this.principal = keytab.principal();
        this.keytab = new FileSystemResource(
            keytab.filepath()
        );
        this.userDetailsService = userDetailsService;
    }

    @Override
    public Map<String, String> headers() {
        return Collections.singletonMap(
            HttpHeaders.WWW_AUTHENTICATE,
            "Negotiate"
        );
    }

    @Override
    public AuthenticationProvider createAuthenticationProvider(
    ) throws Exception {
        final var provider = new KerberosServiceAuthenticationProvider();

        provider.setTicketValidator(this.createKerberosTicketValidator());
        provider.setUserDetailsService(this.userDetailsService);

        return provider;
    }

    @Override
    public Filter createFilter(
        final AuthenticationManager authenticationManager
    ) {
        final var filter = new SpnegoAuthenticationProcessingFilter();

        filter.setAuthenticationManager(authenticationManager);

        return filter;
    }

    private KerberosTicketValidator createKerberosTicketValidator(
    ) throws Exception {
        final var ticketValidator = new SunJaasKerberosTicketValidator();

        ticketValidator.setServicePrincipal(this.principal);
        ticketValidator.setKeyTabLocation(this.keytab);
        ticketValidator.afterPropertiesSet();

        return ticketValidator;
    }
}
