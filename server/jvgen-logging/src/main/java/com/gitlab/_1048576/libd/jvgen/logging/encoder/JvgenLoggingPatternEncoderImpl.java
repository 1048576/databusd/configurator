package com.gitlab._1048576.libd.jvgen.logging.encoder;

import ch.qos.logback.classic.encoder.PatternLayoutEncoder;
import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.core.Context;
import ch.qos.logback.core.encoder.EncoderBase;
import com.gitlab._1048576.libd.jvgen.logging.NotLoggingException;
import java.nio.charset.Charset;

public class JvgenLoggingPatternEncoderImpl extends EncoderBase<ILoggingEvent> {
    private final PatternLayoutEncoder encoder;

    public JvgenLoggingPatternEncoderImpl() {
        this.encoder = new PatternLayoutEncoder();
    }

    @Override
    public byte[] headerBytes() {
        return this.encoder.headerBytes();
    }

    @Override
    public byte[] encode(final ILoggingEvent event) {
        do {
            if (event.getThrowableProxy() == null) {
                break;
            }

            var cause = event.getThrowableProxy().getCause();

            while (cause != null) {
                final var className = cause.getClassName();

                if (className.equals(NotLoggingException.class.getName())) {
                    return new byte[0];
                }

                cause = cause.getCause();
            }
        } while (false);

        return this.encoder.encode(event);
    }

    @Override
    public byte[] footerBytes() {
        return this.encoder.footerBytes();
    }

    @Override
    public void start() {
        this.encoder.start();
        super.start();
    }

    @Override
    public void setContext(final Context context) {
        this.encoder.setContext(context);
    }

    public void setPattern(final String pattern) {
        this.encoder.setPattern(pattern);
    }

    public void setCharset(final Charset charset) {
        this.encoder.setCharset(charset);
    }
}
