package com.gitlab._1048576.databusd.worker;

import java.util.Map;

public interface DatabusWorkerProcessor {
    public void process(
        final byte[] body,
        final String contentType,
        final Map<String, ? extends Object> headers
    ) throws Exception;
}
