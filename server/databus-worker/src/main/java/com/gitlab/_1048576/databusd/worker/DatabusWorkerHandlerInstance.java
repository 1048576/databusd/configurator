package com.gitlab._1048576.databusd.worker;

public interface DatabusWorkerHandlerInstance {
    public void stop() throws Exception;
}
