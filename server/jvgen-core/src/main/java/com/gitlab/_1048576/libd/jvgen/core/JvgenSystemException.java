package com.gitlab._1048576.libd.jvgen.core;

public class JvgenSystemException extends RuntimeException {
    public JvgenSystemException(final String message) {
        super(message);
    }

    public JvgenSystemException(final Throwable cause) {
        super(cause);
    }
}
