package com.gitlab._1048576.libd.jvgen.core;

public class JvgenUserException extends Exception {
    public JvgenUserException(final String message) {
        super(message);
    }
}
