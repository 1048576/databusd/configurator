package com.gitlab._1048576.databusd.gateway.http;

import java.util.Map;

public interface DatabusHttpGatewayProcessor {
    public String process(
        final byte[] body,
        final String contentType,
        final String routingKey,
        final Map<String, ? extends Object> headers
    ) throws Exception;
}
