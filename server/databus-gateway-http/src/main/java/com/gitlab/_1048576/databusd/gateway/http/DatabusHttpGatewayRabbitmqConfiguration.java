package com.gitlab._1048576.databusd.gateway.http;

import com.gitlab._1048576.libd.jvgen.rabbitmq.JvgenRabbitmqBroker;

public interface DatabusHttpGatewayRabbitmqConfiguration {
    public JvgenRabbitmqBroker createBroker() throws Exception;

    public String exchange();
}
