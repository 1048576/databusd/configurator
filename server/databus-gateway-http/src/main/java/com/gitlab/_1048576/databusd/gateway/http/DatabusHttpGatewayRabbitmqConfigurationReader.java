package com.gitlab._1048576.databusd.gateway.http;

import com.gitlab._1048576.libd.jvgen.cfg.configuration.JvgenCfgConfigurationObjectNode;

public interface DatabusHttpGatewayRabbitmqConfigurationReader {
    public DatabusHttpGatewayRabbitmqConfiguration read(
        final JvgenCfgConfigurationObjectNode node
    ) throws Exception;
}
