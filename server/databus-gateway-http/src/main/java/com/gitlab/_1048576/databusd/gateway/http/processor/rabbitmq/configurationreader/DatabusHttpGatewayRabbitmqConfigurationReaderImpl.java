package com.gitlab._1048576.databusd.gateway.http.processor.rabbitmq.configurationreader;

import com.gitlab._1048576.databusd.gateway.http.DatabusHttpGatewayRabbitmqConfiguration;
import com.gitlab._1048576.databusd.gateway.http.DatabusHttpGatewayRabbitmqConfigurationReader;
import com.gitlab._1048576.libd.jvgen.cfg.configuration.JvgenCfgConfigurationObjectNode;
import com.gitlab._1048576.libd.jvgen.rabbitmq.JvgenRabbitmqBroker;
import com.gitlab._1048576.libd.jvgen.rabbitmq.configurationreader.JvgenRabbitmqBrokerConfigurationReaderImpl;

public class DatabusHttpGatewayRabbitmqConfigurationReaderImpl implements DatabusHttpGatewayRabbitmqConfigurationReader {
    @Override
    public DatabusHttpGatewayRabbitmqConfiguration read(
        final JvgenCfgConfigurationObjectNode node
    ) throws Exception {
        final var rabbitmqNode = node.detachObjectNode(
            "rabbitmq"
        );

        final var brokerConfigurationReader = new JvgenRabbitmqBrokerConfigurationReaderImpl();

        final var brokerConfiguration = brokerConfigurationReader.read(
            rabbitmqNode
        );

        final var exchange = rabbitmqNode.detachTextNode("exchange");

        rabbitmqNode.close();

        return new DatabusHttpGatewayRabbitmqConfiguration() {
            @Override
            public JvgenRabbitmqBroker createBroker() throws Exception {
                return brokerConfiguration.create();
            }

            @Override
            public String exchange() {
                return exchange;
            }
        };
    }
}
