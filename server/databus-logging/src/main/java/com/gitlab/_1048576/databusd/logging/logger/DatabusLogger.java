package com.gitlab._1048576.databusd.logging.logger;

import java.util.Map;

public interface DatabusLogger {
    public void messageProcessingCompleted(
        final String trackingId,
        final String routingKey,
        final Map<String, String> headers,
        final Integer size
    );

    public void messageProcessingFailed(
        final String routingKey,
        final Map<String, String> headers,
        final String reason
    );
}
