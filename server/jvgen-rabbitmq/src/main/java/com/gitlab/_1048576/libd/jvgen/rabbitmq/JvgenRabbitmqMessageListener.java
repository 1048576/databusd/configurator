package com.gitlab._1048576.libd.jvgen.rabbitmq;

import java.util.Map;

public interface JvgenRabbitmqMessageListener {
    public void onMessage(
        final String trackingId,
        final byte[] body,
        final String contentType,
        final String routingKey,
        final Map<String, String> headers
    ) throws Exception;
}
