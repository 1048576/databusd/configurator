package com.gitlab._1048576.libd.jvgen.rabbitmq;

import com.gitlab._1048576.libd.jvgen.core.JvgenUserException;
import com.gitlab._1048576.libd.jvgen.rabbitmq.consumer.JvgenRabbitmqConsumerImpl;
import com.gitlab._1048576.libd.jvgen.rabbitmq.publisher.JvgenRabbitmqPublisherImpl;
import java.security.MessageDigest;
import org.springframework.amqp.rabbit.connection.CachingConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;

public final class JvgenRabbitmqBrokerImpl implements JvgenRabbitmqBroker {
    private final CachingConnectionFactory connectionFactory;
    private final MessageDigest digestAlgorithm;

    public JvgenRabbitmqBrokerImpl(
        final CachingConnectionFactory connectionFactory
    ) throws Exception {
        this.connectionFactory = connectionFactory;
        this.digestAlgorithm = MessageDigest.getInstance("SHA-256");
    }

    @Override
    public JvgenRabbitmqPublisher createPublisher(
        final String name
    ) throws Exception {
        this.declareExchange(name);

        final var rabbitTemplate = new RabbitTemplate(this.connectionFactory);

        return new JvgenRabbitmqPublisherImpl(
            this::createTrackingId,
            name,
            rabbitTemplate
        );
    }

    @Override
    public JvgenRabbitmqConsumer createConsumer(
        final String queue,
        final JvgenRabbitmqMessageListener messageListener
    ) {
        return new JvgenRabbitmqConsumerImpl(
            this::createTrackingId,
            this.connectionFactory,
            queue,
            messageListener
        );
    }

    private String createTrackingId(final byte[] body) {
        final var result = new StringBuilder(body.length);

        for (final byte b : this.digestAlgorithm.digest(body)) {
            result.append(Integer.toHexString(Byte.toUnsignedInt(b)));
        }

        return result.toString();
    }

    private void declareExchange(final String name) throws Exception {
        final var connection = this.connectionFactory.createConnection();
        final var channel = connection.createChannel(false);

        try {
            channel.exchangeDeclarePassive(name);
        } catch (final Exception e) {
            if (e.getCause() != null) {
                throw new JvgenUserException(
                    String.format(
                        "Exchange [%s] declare problem detected: [%s]",
                        name,
                        e.getCause().getMessage()
                    )
                );
            } else {
                throw e;
            }
        } finally {
            channel.close();
            connection.close();
        }
    }
}
