package com.gitlab._1048576.libd.jvgen.rabbitmq.publisher;


import com.gitlab._1048576.libd.jvgen.core.JvgenUserException;
import com.gitlab._1048576.libd.jvgen.rabbitmq.JvgenRabbitmqPublisher;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.function.Function;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageBuilder;
import org.springframework.amqp.rabbit.connection.CorrelationData;
import org.springframework.amqp.rabbit.core.RabbitTemplate;

public final class JvgenRabbitmqPublisherImpl implements JvgenRabbitmqPublisher {
    private final Function<byte[], String> createTrackingIdFn;
    private final RabbitTemplate rabbitTemplate;
    private final String exchange;

    public JvgenRabbitmqPublisherImpl(
        final Function<byte[], String> createTrackingIdFn,
        final String exchange,
        final RabbitTemplate rabbitTemplate
    ) throws Exception {
        this.createTrackingIdFn = createTrackingIdFn;
        this.exchange = exchange;
        this.rabbitTemplate = rabbitTemplate;
    }

    @Override
    public String publish(
        final byte[] body,
        final String contentType,
        final String routingKey,
        final Map<String, ? extends Object> headers
    ) throws Exception {
        final var trackingId = this.createTrackingIdFn.apply(body);
        final var message = this.createMessage(
            body,
            contentType,
            routingKey,
            headers,
            routingKey
        );
        final var correlationData = new CorrelationData();

        this.send(routingKey, message, correlationData);
        this.waitDeliveryConfirmation(correlationData);

        return trackingId;
    }

    private Message createMessage(
        final byte[] body,
        final String contentType,
        final String routingKey,
        final Map<String, ? extends Object> headers,
        final String trackingId
    ) {
        final var messageBuilder = MessageBuilder
            .withBody(body)
            .setContentType(contentType)
            .setMessageId(trackingId);

        for (final var header : headers.entrySet()) {
            messageBuilder.setHeader(header.getKey(), header.getValue());
        }

        return messageBuilder.build();
    }

    private void send(
        final String routingKey,
        final Message message,
        final CorrelationData correlationData
    ) {
        this.rabbitTemplate.setMandatory(true);
        this.rabbitTemplate.convertAndSend(
            this.exchange,
            routingKey,
            message,
            correlationData
        );
    }

    private void waitDeliveryConfirmation(
        final CorrelationData correlationData
    ) throws Exception {
        final var future = correlationData.getFuture();
        final var confirm = future.get(5, TimeUnit.SECONDS);

        if (!confirm.isAck()) {
            throw new RuntimeException(confirm.getReason());
        } else if (correlationData.getReturned() != null) {
            throw new JvgenUserException("Unknown route");
        }
    }
}
