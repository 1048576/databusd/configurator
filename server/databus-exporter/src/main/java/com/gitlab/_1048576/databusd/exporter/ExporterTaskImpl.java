package com.gitlab._1048576.databusd.exporter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ExporterTaskImpl implements Runnable {
    private final Logger logger;

    public ExporterTaskImpl(
        final String entityName,
        final String routingKey
    ) {
        this.logger = LoggerFactory.getLogger(
            String.format(
                "[%s][%s]",
                entityName,
                routingKey
            )
        );
    }

    @Override
    public void run() {
        this.logger.info("send");
    }
}
