package com.gitlab._1048576.databusd.worker.handler.rabbitmq;

import com.gitlab._1048576.libd.jvgen.cfg.configuration.JvgenCfgConfigurationObjectNode;

public interface DatabusWorkerRabbitmqHandlerConfigurationReader {
    public DatabusWorkerRabbitmqHandlerConfiguration read(
        final JvgenCfgConfigurationObjectNode node
    ) throws Exception;
}
