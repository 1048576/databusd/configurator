package com.gitlab._1048576.databusd.worker.http;

public interface DatabusHttpWorkerRabbitmqHandlersConfiguration {
    public Iterable<DatabusHttpWorkerRabbitmqHandlerConfiguration> handlers();
}
