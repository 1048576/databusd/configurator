package com.gitlab._1048576.databusd.worker.http;

import java.util.Map;

public interface DatabusHttpWorkerKeytabsConfiguration {
    public Map<String, DatabusHttpWorkerKeytabConfiguration> keytabs();
}
