package com.gitlab._1048576.databusd.worker.http;

public interface DatabusHttpWorkerEndpointConfiguration {
    public String name();

    public String url();

    public String keytab();
}
