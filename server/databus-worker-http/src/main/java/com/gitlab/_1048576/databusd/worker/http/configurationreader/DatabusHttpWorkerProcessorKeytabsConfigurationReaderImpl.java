package com.gitlab._1048576.databusd.worker.http.configurationreader;

import com.gitlab._1048576.databusd.worker.http.DatabusHttpWorkerKeytabConfiguration;
import com.gitlab._1048576.databusd.worker.http.DatabusHttpWorkerKeytabsConfiguration;
import com.gitlab._1048576.databusd.worker.http.DatabusHttpWorkerProcessorKeytabsConfigurationReader;
import com.gitlab._1048576.libd.jvgen.cfg.configuration.JvgenCfgConfigurationObjectListNode;
import com.gitlab._1048576.libd.jvgen.cfg.configuration.JvgenCfgConfigurationObjectNode;
import com.gitlab._1048576.libd.jvgen.core.JvgenUserException;
import com.gitlab._1048576.libd.jvgen.kerberos.configurationreader.JvgenKerberosKeytabConfigurationReaderImpl;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class DatabusHttpWorkerProcessorKeytabsConfigurationReaderImpl implements DatabusHttpWorkerProcessorKeytabsConfigurationReader {
    @Override
    public DatabusHttpWorkerKeytabsConfiguration read(
        final JvgenCfgConfigurationObjectNode node
    ) throws Exception {
        final var keytabsNode = node.detachObjectListNode(
            "keytabs"
        );

        final var keytabs = this.createKeytabs(
            keytabsNode
        );

        keytabsNode.close();

        return new DatabusHttpWorkerKeytabsConfiguration() {
            @Override
            public Map<String, DatabusHttpWorkerKeytabConfiguration> keytabs() {
                return Collections.unmodifiableMap(keytabs);
            }
        };
    }

    private Map<String, DatabusHttpWorkerKeytabConfiguration> createKeytabs(
        final JvgenCfgConfigurationObjectListNode node
    ) throws Exception {
        final var keytabs = new HashMap<String, DatabusHttpWorkerKeytabConfiguration>();

        while (!node.isEmpty()) {
            final var keytabNode = node.detachObjectNode();

            final var keytab = this.createKeytab(keytabNode);

            keytabNode.close();

            if (keytabs.put(keytab.name(), keytab) != null) {
                throw new JvgenUserException(
                    String.format(
                        "Keytab [%s] is not unique",
                        keytab.name()
                    )
                );
            }
        }

        return keytabs;
    }

    private DatabusHttpWorkerKeytabConfiguration createKeytab(
        final JvgenCfgConfigurationObjectNode node
    ) throws Exception {
        final var keytabConfigurationReader = new JvgenKerberosKeytabConfigurationReaderImpl();

        final var name = node.detachTextNode("name");

        final var keytabConfigution = keytabConfigurationReader.read(node);

        return new DatabusHttpWorkerKeytabConfiguration() {
            @Override
            public String name() {
                return name;
            }

            @Override
            public String principal() {
                return keytabConfigution.principal();
            }

            @Override
            public String filepath() {
                return keytabConfigution.filepath();
            }
        };
    }
}
